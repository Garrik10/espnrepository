/*
 * CH.cc
 *
 *  Created on: 11 avr. 2020
 *      Author: Garrik Brel
 */




#include <stdio.h>
#include <string.h>
#include <omnetpp.h>
#include "espn_m.h"

using namespace omnetpp;


class CH : public cSimpleModule
{
  private:
    long residualEnergy;
    long numSent;
    long numReceived;
    long receivedByCH;
    cHistogram hopCountStats;
    cOutVector hopCountVector;
    cHistogram residualEnergyStats;
    cOutVector residualEnergyVector;

  protected:
    virtual char *cryptMessage(char *msg);
    virtual EspnMsg *generateMessage(const char * src, const char * dest);
    virtual void forwardMessage(EspnMsg *msg, int gate);
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;

    // The finish() function is called by OMNeT++ at the end of the simulation:
    virtual void finish() override;
};

Define_Module(CH);

void CH::initialize()
{
    receivedByCH = 0;
    residualEnergy = 1000;
    WATCH(receivedByCH);
    WATCH(residualEnergy);

    hopCountStats.setName("hopCountStats");
    hopCountVector.setName("HopCount");

    residualEnergyStats.setName("residualEnergyStats");
    residualEnergyVector.setName("residualEnergy");
}

void CH::handleMessage(cMessage *msg)
{
    EspnMsg *ttmsg = check_and_cast<EspnMsg *>(msg);
    receivedByCH = receivedByCH + 1;
    residualEnergy = residualEnergy - (residualEnergy*0.3/100);;

    // Message arrived
    int hopcount = ttmsg->getHopCount();

    // update statistics.
    numReceived++;
    hopCountVector.record(hopcount);
    hopCountStats.collect(hopcount);

    residualEnergyVector.record(residualEnergy);
    residualEnergyStats.collect(residualEnergy);

    //delete ttmsg;
    if (strcmp("ch0", ttmsg->getDestination()) == 0) {
        if (receivedByCH < 2) {
            char text[32];
            sprintf(text, "messages arrived : %d messages", receivedByCH);
            bubble(text);
            //bubble("ARRIVED, waiting for another message!");
        }
        else if (receivedByCH == 2) {
            // Generate another one.
            char text[32];
            sprintf(text, "messages arrived : %d messages", receivedByCH,"sending to the base station");
            bubble(text);
            EV << "Generating another message: ";
            EspnMsg *newmsg = generateMessage("cho","baseStation");
            EV << newmsg << endl;
            forwardMessage(newmsg,4);
            receivedByCH = 0;
            numSent++;
        }

    }

    else if (strcmp("ch1", ttmsg->getDestination()) == 0) {
        if (receivedByCH < 2) {
            char text[32];
            sprintf(text, "messages arrived : %d messages", receivedByCH);
            bubble(text);
            //bubble("ARRIVED, waiting for another message!");
        }
        else if (receivedByCH == 2) {
            // Generate another one.
            char text[32];
            sprintf(text, "messages arrived : %d messages. sending to the base station", receivedByCH);
            bubble(text);
            EV << "Generating another message: ";
            EspnMsg *newmsg = generateMessage("ch1","ch0");
            EV << newmsg << endl;
            forwardMessage(newmsg,4);
            receivedByCH = 0;
            numSent++;
        }
    }

    else if (strcmp("ch2", ttmsg->getDestination()) == 0) {
        if (receivedByCH < 2) {
            char text[32];
            sprintf(text, "messages arrived : %d messages", receivedByCH);
            bubble(text);
            //bubble("ARRIVED, waiting for another message!");
        }
        else if (receivedByCH == 2) {
            // Generate another one.
            char text[32];
            sprintf(text, "messages arrived : %d messages. sending to the base station", receivedByCH);
            bubble(text);
            EV << "Generating another message: ";
            EspnMsg *newmsg = generateMessage("ch2","ch1");
            EV << newmsg << endl;
            forwardMessage(newmsg,4);
            receivedByCH = 0;
            numSent++;
        }
    }

    else if (strcmp("ch3", ttmsg->getDestination()) == 0) {
        if (receivedByCH < 2) {
            char text[32];
            sprintf(text, "messages arrived : %d messages", receivedByCH);
            bubble(text);
            //bubble("ARRIVED, waiting for another message!");
        }
        else if (receivedByCH == 2) {
            // Generate another one.
            char text[32];
            sprintf(text, "messages arrived : %d messages. sending to the base station", receivedByCH);
            bubble(text);
            EV << "Generating another message: ";
            EspnMsg *newmsg = generateMessage("ch3","ch0");
            EV << newmsg << endl;
            forwardMessage(newmsg,4);
            receivedByCH = 0;
            numSent++;
        }
    }

    else if (strcmp("ch4", ttmsg->getDestination()) == 0) {
        if (receivedByCH < 2) {
            char text[32];
            sprintf(text, "messages arrived : %d messages", receivedByCH);
            bubble(text);
            //bubble("ARRIVED, waiting for another message!");
        }
        else if (receivedByCH == 2) {
            // Generate another one.
            char text[32];
            sprintf(text, "messages arrived : %d messages. sending to the base station", receivedByCH);
            bubble(text);
            EV << "Generating another message: ";
            EspnMsg *newmsg = generateMessage("ch4","ch1");
            EV << newmsg << endl;
            forwardMessage(newmsg,4);
            receivedByCH = 0;
            numSent++;
        }
    }

    else if (strcmp("ch5", ttmsg->getDestination()) == 0) {
        if (receivedByCH < 2) {
            char text[32];
            sprintf(text, "messages arrived : %d messages", receivedByCH);
            bubble(text);
            //bubble("ARRIVED, waiting for another message!");
        }
        else if (receivedByCH == 2) {
            // Generate another one.
            char text[32];
            sprintf(text, "messages arrived : %d messages. sending to the base station", receivedByCH);
            bubble(text);
            EV << "Generating another message: ";
            EspnMsg *newmsg = generateMessage("ch5","ch4");
            EV << newmsg << endl;
            forwardMessage(newmsg,4);
            receivedByCH = 0;
            numSent++;
        }
    }
}

EspnMsg *CH::generateMessage(const char * src, const char * dest)
{
    char msgname[40];
    sprintf(msgname, cryptMessage("message_to_send"));

    // Create message object and set source and destination field.
    EspnMsg *msg = new EspnMsg(msgname);
    msg->setSource(src);
    msg->setDestination(dest);
    return msg;
}


void CH::forwardMessage(EspnMsg *msg, int gate)
{
    // Increment hop count.
    residualEnergy = residualEnergy - (residualEnergy*0.5/100);;
    residualEnergyVector.record(residualEnergy);
    residualEnergyStats.collect(residualEnergy);
    msg->setHopCount(msg->getHopCount()+1);
    send(msg, "gate$o", gate);
}

char *CH::cryptMessage(char *msg)
{
    char cryptedMessage[sizeof(msg)];

    for (int i=0; i<strlen(msg) ; i++)
    {
        cryptedMessage[i] = msg[i] ^ i;
    }

    cryptedMessage[strlen(msg)] = NULL;
    residualEnergy = residualEnergy - (residualEnergy*0.3/100);
    residualEnergyVector.record(residualEnergy);
    residualEnergyStats.collect(residualEnergy);
    return cryptedMessage;
}

void CH::finish()
{
    // This function is called by OMNeT++ at the end of the simulation.
    EV << "Sent:     " << numSent << endl;
    EV << "Received: " << numReceived << endl;
    EV << "Hop count, min:    " << hopCountStats.getMin() << endl;
    EV << "Hop count, max:    " << hopCountStats.getMax() << endl;
    EV << "Hop count, mean:   " << hopCountStats.getMean() << endl;
    EV << "Hop count, stddev: " << hopCountStats.getStddev() << endl;

    recordScalar("#sent", numSent);
    recordScalar("#received", numReceived);

    hopCountStats.recordAs("hop count");
    residualEnergyStats.recordAs("residual_energy");
}
