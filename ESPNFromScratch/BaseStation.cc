/*
 * espn.cc
 *
 *  Created on: 6 avr. 2020
 *      Author: Garrik Brel
 */
#include <stdio.h>
#include <string.h>
#include <omnetpp.h>
#include "espn_m.h"

using namespace omnetpp;


class BaseStation : public cSimpleModule
{
  private:
    long numSent;
    long numReceived;
    cHistogram hopCountStats;
    cOutVector hopCountVector;

  protected:
    virtual void forwardMessage(EspnMsg *msg);
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;

    // The finish() function is called by OMNeT++ at the end of the simulation:
    virtual void finish() override;
};

Define_Module(BaseStation);

void BaseStation::initialize()
{
    bubble("Waiting for messages to arrive");
}

void BaseStation::handleMessage(cMessage *msg)
{
    EspnMsg *ttmsg = check_and_cast<EspnMsg *>(msg);

    if (strcmp("baseStation", ttmsg->getDestination()) == 0) {
        numReceived++;
        char text[32];
        sprintf(text, "Number of messages received by the base station : %d messages", numReceived);
        bubble(text);

        // Message arrived
        int hopcount = ttmsg->getHopCount();
        EV << "Message " << ttmsg << " arrived after " << hopcount << " hops.\n";

        // update statistics.

        hopCountVector.record(hopcount);
        hopCountStats.collect(hopcount);

        delete ttmsg;
    }
    else {
        // We need to forward the message.
        forwardMessage(ttmsg);
    }
}

void BaseStation::forwardMessage(EspnMsg *msg)
{
    // Increment hop count.
    msg->setHopCount(msg->getHopCount()+1);

    // Same routing as before: random gate.
    int n = gateSize("gate");
    int k = intuniform(0, n-1);

    EV << "Forwarding message " << msg << " on gate[" << k << "]\n";
    send(msg, "gate$o", k);
}

void BaseStation::finish()
{
    // This function is called by OMNeT++ at the end of the simulation.
    EV << "Sent:     " << numSent << endl;
    EV << "Received: " << numReceived << endl;
    EV << "Hop count, min:    " << hopCountStats.getMin() << endl;
    EV << "Hop count, max:    " << hopCountStats.getMax() << endl;
    EV << "Hop count, mean:   " << hopCountStats.getMean() << endl;
    EV << "Hop count, stddev: " << hopCountStats.getStddev() << endl;

    recordScalar("#sent", numSent);
    recordScalar("#received", numReceived);

    hopCountStats.recordAs("hop count");
}
