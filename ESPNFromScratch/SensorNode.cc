/*
 * Node.cc
 *
 *  Created on: 6 avr. 2020
 *      Author: Garrik Brel
 */


#include <stdio.h>
#include <string.h>
#include <omnetpp.h>
#include "espn_m.h"

using namespace omnetpp;


class SensorNode : public  cSimpleModule
{
  private:
    long residualEnergy;
    long numSent;
    long numReceived;
    cHistogram hopCountStats;
    cOutVector hopCountVector;
    cHistogram residualEnergyStats;
    cOutVector residualEnergyVector;

  protected:
    virtual char *cryptMessage(char *msg);
    virtual EspnMsg *generateMessage(const char * src, const char * dest);
    virtual void forwardMessage(EspnMsg *msg);
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;

    // The finish() function is called by OMNeT++ at the end of the simulation:
    virtual void finish() override;
};

Define_Module(SensorNode);

void SensorNode::initialize()
{
    // Initialize variables
    numSent = 0;
    numReceived = 0;
    residualEnergy = 1000;
    WATCH(numSent);
    WATCH(numReceived);
    WATCH(residualEnergy);

    hopCountStats.setName("hopCountStats");
    hopCountVector.setName("HopCount");

    residualEnergyStats.setName("residualEnergyStats");
    residualEnergyVector.setName("residualEnergy");

    for (int i = 0; i < 2; i++) {
      if (i == 0) {
          // Module 0 sends the first message
              if (strcmp("node0", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node0", "ch0");
                  scheduleAt(0.0, msg);
              }

              if (strcmp("node1", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node1", "ch0");
                  scheduleAt(15, msg);
              }

              if (strcmp("node2", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node2", "ch1");
                  scheduleAt(5, msg);
              }

              if (strcmp("node3", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node3", "ch1");
                  scheduleAt(8, msg);
              }

              if (strcmp("node4", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node4", "ch2");
                  scheduleAt(0.8, msg);
              }

              if (strcmp("node5", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node5", "ch2");
                  scheduleAt(4, msg);
              }

              if (strcmp("node6", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node6", "ch0");
                  scheduleAt(25, msg);
              }

              if (strcmp("node7", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node7", "ch0");
                  scheduleAt(30, msg);
              }

              if (strcmp("node8", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node8", "ch1");
                  scheduleAt(14, msg);
              }

              if (strcmp("node9", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node9", "ch1");
                  scheduleAt(15, msg);
              }

              if (strcmp("node10", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node10", "ch2");
                  scheduleAt(8, msg);
              }

              if (strcmp("node11", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node11", "ch2");
                  scheduleAt(10, msg);
              }

              if (strcmp("node12", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node12", "ch3");
                  scheduleAt(2, msg);
              }

              if (strcmp("node13", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node13", "ch3");
                  scheduleAt(5, msg);
              }

              if (strcmp("node14", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node14", "ch4");
                  scheduleAt(8, msg);
              }

              if (strcmp("node15", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node15", "ch4");
                  scheduleAt(15, msg);
              }

              if (strcmp("node16", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node16", "ch5");
                  scheduleAt(4, msg);
              }

              if (strcmp("node17", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node17", "ch5");
                  scheduleAt(8, msg);
              }

              if (strcmp("node18", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node18", "ch3");
                  scheduleAt(7, msg);
              }

              if (strcmp("node19", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node19", "ch3");
                  scheduleAt(10, msg);
              }

              if (strcmp("node20", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node20", "ch4");
                  scheduleAt(20, msg);
              }

              if (strcmp("node21", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node21", "ch4");
                  scheduleAt(25, msg);
              }

              if (strcmp("node22", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node22", "ch5");
                  scheduleAt(10, msg);
              }

              if (strcmp("node23", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node23", "ch5");
                  scheduleAt(15, msg);
              }
      }

      if (i == 1 ) {
          // Module 0 sends the first message
              if (strcmp("node0", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node0", "ch0");
                  scheduleAt(50, msg);
              }

              if (strcmp("node1", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node1", "ch0");
                  scheduleAt(35, msg);
              }

              if (strcmp("node2", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node2", "ch1");
                  scheduleAt(35, msg);
              }

              if (strcmp("node3", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node3", "ch1");
                  scheduleAt(40, msg);
              }

              if (strcmp("node4", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node4", "ch2");
                  scheduleAt(30, msg);
              }

              if (strcmp("node5", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node5", "ch2");
                  scheduleAt(55, msg);
              }

              if (strcmp("node6", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node6", "ch0");
                  scheduleAt(40, msg);
              }

              if (strcmp("node7", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node7", "ch0");
                  scheduleAt(60, msg);
              }

              if (strcmp("node8", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node8", "ch1");
                  scheduleAt(50, msg);
              }

              if (strcmp("node9", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node9", "ch1");
                  scheduleAt(60, msg);
              }

              if (strcmp("node10", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node10", "ch2");
                  scheduleAt(35, msg);
              }

              if (strcmp("node11", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node11", "ch2");
                  scheduleAt(40, msg);
              }

              if (strcmp("node12", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node12", "ch3");
                  scheduleAt(32, msg);
              }

              if (strcmp("node13", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node13", "ch3");
                  scheduleAt(40, msg);
              }

              if (strcmp("node14", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node14", "ch4");
                  scheduleAt(48, msg);
              }

              if (strcmp("node15", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node15", "ch4");
                  scheduleAt(39, msg);
              }

              if (strcmp("node16", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node16", "ch5");
                  scheduleAt(30, msg);
              }

              if (strcmp("node17", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node17", "ch5");
                  scheduleAt(35, msg);
              }

              if (strcmp("node18", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node18", "ch3");
                  scheduleAt(41, msg);
              }

              if (strcmp("node19", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node19", "ch3");
                  scheduleAt(50, msg);
              }

              if (strcmp("node20", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node20", "ch4");
                  scheduleAt(60, msg);
              }

              if (strcmp("node21", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node21", "ch4");
                  scheduleAt(65, msg);
              }

              if (strcmp("node22", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node22", "ch5");
                  scheduleAt(50, msg);
              }

              if (strcmp("node23", getName()) == 0) {
                  EspnMsg *msg = generateMessage("node23", "ch5");
                  scheduleAt(45, msg);
              }
      }
    }


}

void SensorNode::handleMessage(cMessage *msg)
{
    EspnMsg *ttmsg = check_and_cast<EspnMsg *>(msg);
    forwardMessage(ttmsg);
}

EspnMsg *SensorNode::generateMessage(const char * src, const char * dest)
{
    char letters[26] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q',
    'r','s','t','u','v','w','x',
    'y','z'};
    char ran [5];
    for (int i=0;i<5;i++)
        ran[i] = letters[rand() % 26];

    char msgname[20];
    sprintf(msgname, cryptMessage(ran));

    // Create message object and set source and destination field.
    EspnMsg *msg = new EspnMsg(msgname);
    msg->setSource(src);
    msg->setDestination(dest);
    residualEnergy = residualEnergy - (residualEnergy*0.5/100);
    residualEnergyVector.record(residualEnergy);
    residualEnergyStats.collect(residualEnergy);
    return msg;
}

void SensorNode::forwardMessage(EspnMsg *msg)
{
    // Increment hop count.
    msg->setHopCount(msg->getHopCount()+1);

    // Same routing as before: random gate.
    int n = gateSize("gate");
    int k = intuniform(0, n-1);

    EV << "Forwarding message " << msg << " on gate[" << k << "]\n";
    send(msg, "gate$o", k);
}

char *SensorNode::cryptMessage(char *msg)
{
    char cryptedMessage[sizeof(msg)];

    for (int i=0; i<strlen(msg) ; i++)
    {
        cryptedMessage[i] = msg[i] ^ i;
    }

    cryptedMessage[strlen(msg)] = NULL;
    residualEnergy = residualEnergy - (residualEnergy*0.2/100);
    residualEnergyVector.record(residualEnergy);
    residualEnergyStats.collect(residualEnergy);
    return cryptedMessage;
}

void SensorNode::finish()
{
    // This function is called by OMNeT++ at the end of the simulation.
    EV << "Sent:     " << numSent << endl;
    EV << "Received: " << numReceived << endl;
    EV << "Hop count, min:    " << hopCountStats.getMin() << endl;
    EV << "Hop count, max:    " << hopCountStats.getMax() << endl;
    EV << "Hop count, mean:   " << hopCountStats.getMean() << endl;
    EV << "Hop count, stddev: " << hopCountStats.getStddev() << endl;

    recordScalar("#sent", numSent);
    recordScalar("#received", numReceived);
    residualEnergyStats.recordAs("residual_energy");
}


