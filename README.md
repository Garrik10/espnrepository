In order to run these simulations on your local machine, you need to follow theses steps:

1- Download and install Omnet++ set up (version 5.6.1) accessible via the link: https://omnetpp.org/download/ 
    Notice that you have to choose the version of Omnett++ corresponding to your operating system.
	
2- Clone the project espn from bitbucket by typing in your command line: git clone https://Garrik10@bitbucket.org/Garrik10/espnrepository.git

3- Run omnet++ by following these steps:
	- Open the downloaded omnet++ folder and double-clic on the file mingwenv.cmd
	- The execution of this file just open a new terminal in which you just need to type the command omnetpp
	- The omnet++ IDE is now opened
	
4- Import the recently cloned project and run it
	Notice that, There are 4 branches: master (simulation with 23 nodes), feature/fifty_nodes_simulation_without_cryptography (Simulation with 50 nodes without cryptography management), 
	fifty_nodes_simulation (simulation with 50 nodes with cryptography)  and twenty_three_nodes_without_cryptography (Simulation with 23 nodes without cryptography management).

5- DONE!!!!